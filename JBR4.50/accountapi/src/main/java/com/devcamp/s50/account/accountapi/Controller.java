package com.devcamp.s50.account.accountapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.account.accountapi.model.Account;
import com.devcamp.s50.account.accountapi.model.Customer;

@RestController
public class Controller {
    @CrossOrigin
    @GetMapping("accounts")
    public ArrayList<Account> getAccountsList(){
        //tạo 3 đối tượng khách hàng
        Customer customer1 = new Customer(1,"OHMM",10);
        Customer customer2 = new Customer(2,"XTRA",20);
        Customer customer3 = new Customer(3,"UNIVERSAL",15);
        System.out.println(customer1.toString());
        System.out.println(customer2.toString());
        System.out.println(customer3.toString());

        //tạo 3 đối tượng account
        Account account1 = new Account(1, customer1, 10000);
        Account account2 = new Account(2, customer2, 15000);
        Account account3 = new Account(3, customer3, 20000);
        System.out.println(account1.toString());
        System.out.println(account2.toString());
        System.out.println(account3.toString());

        ArrayList<Account> accountsList = new ArrayList<Account>();
        accountsList.add(account1);
        accountsList.add(account2);
        accountsList.add(account3);
        
        return accountsList;
    }
}
